import Head from 'next/head'
import Image from 'next/image'
import React, {useState} from 'react'


import Link from 'next/link'
import { useRouter } from 'next/router'
import { signIn} from 'next-auth/react'

export default function Login() {

  const router = useRouter()
  const [authState, setAuthState] = useState({
    username: 'vulcano',
    password: ''
  })
  const [pageState, setPageState] = useState({
    error: '',
    processing: false
  })
  const handleFielChange = (e) => {
    setAuthState(old => ({ ...old, [e.target.id]: e.target.value}))
  }

  const handleAuth = async () => {
    setPageState(old => ({...old, processing: true, error: ''}))
    signIn('credentials', {
      ...authState,
      redirect: false
    }).then(response => {
      console.log(response)
      if (response.ok) {
        router.push("/")
      }else {
        setPageState(old => ({ ...old, processing: false, error: response.error}))
      }
    }).catch(error => {
      console.log(error)
      setPageState(old => ({...old, processing: false, error: error.message ?? "Something went wrong!"}))
    })
  }

  return (
    <div className="w-screen h-screen flex flex-row">
   
      <div className="w-2/5 flex flex-col justify-center">

        <div className="w-full h-2/5 flex flex-col justify-center items-center">

          <div className="w-2/3 h-full">
              <label htmlFor="first" className="text-xs mb-1 text-[#3A4567]">Usuario</label>
              <input type='text' className="w-full h-10 rounded-lg bg-slate-200 border-1 border-slate-400 mb-3 px-3" 
              //keyboardType="string"
              //placeholder='Descripcion' 
              value={authState.username}
              onChange={handleFielChange} />
              <label htmlFor="last" className="text-xs mb-1 text-[#3A4567]">Password</label>
              <input className="w-full h-10 rounded-lg bg-slate-200 border-1 border-slate-400 mb-3 px-3"
              //keyboardType="number"
              //placeholder='Descripcion' 
              val={authState.password}
              onChange={handleFielChange} />
              <button disabled={pageState.processing} type="button" className="w-full h-10 rounded-lg bg-blue-700 border-0 text-white text-xs" onClick={handleAuth}>
                    Ingresar
                 
              </button>
          </div>

        </div>
      </div>

    </div>
  )
}


